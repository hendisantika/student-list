package com.hendisantika.studentlist;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = CrudbootApplication.class) // <!--<version>1.3.5.RELEASE</version>-->
@WebAppConfiguration
public class CrudbootApplicationTests {

	@Test
	public void contextLoads() {
	}

}
