# StudentList
Simple CRUD application made with Spring Boot

- Spring Boot
- Thymeleaf
- H2 database
- Bootstrap

Usage (with eclipse):
1. Clone the project
2. Eclipse: File -> Import -> Maven -> Existing Maven Projects
3. Run on terminal `mvn clean spring-boot:run`
4. Navigate to localhost:8080/student-list/

Note! If you need to use some other port you can add server.port property to /resources/application.resources file
For example server.port=8090

## Screenshot

Home Page

![Home Page](img/home.png "Home Page")

Add Student Page

![Add Student Page](img/add.png "Add Student Page")


