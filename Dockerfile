# Packaging a Spring Boot application as a Docker container
FROM bellsoft/liberica-openjdk-debian:21
VOLUME /tmp
ADD /target/student-list-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
